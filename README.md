# hyperlinks-scraper
> An application to scrape hyperlinks of a website. It exports result as a CSV file.

## Build Dependencies

First, make sure that you have NPM 5 or later installed. (It has feature to lock the dependencies version using `package-lock.json` file.)

Then run

```bash
npm install
```

to install this package's dependencies.

## How to run

```bash
HOST_URL=https://medium.com npm start
```

### List of environment variables

  + `WITHOUT_ASYNC` (default `false`) - we have implemented two different solution to maintain the concurrency. One, using `async` library and another, without using that.
  + `HOST_URL` (default `undefined`) - `required` - the target host url
  + `CONCURRENCY_RATE` (default: `5`) - no of parallel requests to host at a time

## Development

### Configure IDE/Editor

Make sure that you have installed [`EditorConfig`](http://editorconfig.org/). I have used it to define coding style, like indentation size, indentation style, etc. 

### Auto-rerun script

For auto rerunning the script on file update, run

```bash
HOST_URL=https://medium.com npm run dev
```
