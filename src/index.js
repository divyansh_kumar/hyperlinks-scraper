const path = require('path');
const listHyperlinksAsync = require('./services/hyperlinks-scraper');
const exportAsCSV = require('./services/export-as-csv');

if (!process.env.HOST_URL) {
  throw Error('HOST_URL env required.');
}

if (!process.env.CONCURRENCY_RATE) {
  process.env.CONCURRENCY_RATE = 5;
}

console.log(`Host URL: ${process.env.HOST_URL}`);
console.log(`Concurrency rate: ${process.env.CONCURRENCY_RATE}`);

const fields = [
  {
      label: 'Hyperlink',
      value: (hyperlink) => hyperlink,
  },
];

listHyperlinksAsync(process.env.HOST_URL, process.env.CONCURRENCY_RATE)
  .then((hyperlinks) => {
    console.log('Exporting CSV...');
    exportAsCSV(
      Array.from(hyperlinks),
      fields,
      path.resolve(__dirname, '../', `hyperlinks-${Date.now()}.csv`)
    );
    process.exit();
  });
