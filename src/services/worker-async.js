const queue = require('async/queue');

/**
 * It helps in controlling the worker concurrency.
 */
class WorkerController {

  /**
   * @constructor
   *
   * @param {Function} worker - task performer function
   * @param {number} concurrency - the concurrency level
   */
  constructor(worker, concurrency = 5) {
    this._queue = queue((task, callback) => {
      worker(task)
        .then((result) => callback(null, result))
        .catch(callback);
    }, concurrency);
  }

  /**
   * Adds task into queue
   *
   * @param {any} task - an input for worker to perform a task
   * @return {Promise} - resolves with result of the task
   */
  push(task) {
    return new Promise((resolve, reject) =>
      this._queue.push(task, (err, result) =>
        err ? reject(err) : resolve(result))
    );
  }

  /**
   * Assigns callback which will invoke if worker has no more task to perform
   *
   * @param {Function} callback - the callback function
   */
  onDrain(callback) {
    this._queue.drain = callback;
  }

}

module.exports = WorkerController;
