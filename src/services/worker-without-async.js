const Buckets = require('buckets-js');

/**
 * Makes task compatible to the worker queue.
 */
class QueueableTask {

  /**
   * @constructor
   *
   * @param {Function} worker - task performer function
   * @param {any} task - an input for worker to perform a task
   * @param {Function} resolve - on success callback
   * @param {Function} reject - on failure callback
   */
  constructor(worker, task, resolve, reject) {
    this.task = task;

    this._worker = worker;
    this._resolve = resolve;
    this._reject = reject;
  }

  /**
   * Performs task
   *
   * @param {Function} onComplete - on task complete callback
   */
  perform(onComplete) {
    this._worker(this.task)
      .then((result) => {
        onComplete();
        return result;
      })
      .then(this._resolve)
      .catch(this._reject);
  }

}

/**
 * It helps in controlling the worker concurrency.
 */
class WorkerController {

  /**
   * @constructor
   *
   * @param {Function} worker - task performer function
   * @param {number} concurrency - the concurrency level
   */
  constructor(worker, concurrency = 5) {
    this.worker = worker;
    this.concurrency = concurrency;

    this._currentLoad = 0;
    this._queue = Buckets.Queue();
  }

  /**
   * Performs task and maintains concurrency level
   */
  _performTask() {
    if (this._queue.isEmpty()) {
      return this._onDrain();
    }
    // Check the current concurrency level
    if (this._currentLoad < this.concurrency) {
      // increase current concurrency level before starting the task
      this._currentLoad++;
      this._queue.dequeue().perform(() => {
        // decrease current concurrency level after task completion
        this._currentLoad--;
        // go for next task in the queue check
        this._performTask();
      });
    }
  }

  /**
   * Adds task into queue
   *
   * @param {any} task - an input for worker to perform a task
   * @return {Promise} - resolves with result of the task
   */
  push(task) {
    return new Promise((resolve, reject) => {
      this._queue.add(new QueueableTask(this.worker, task, resolve, reject));
      // Trigger task performer.
      this._performTask();
    });
  }

  /**
   * Assigns callback which will invoke if worker has no more task to perform
   *
   * @param {Function} callback - the callback function
   */
  onDrain(callback) {
    this._onDrain = callback;
  }

}

module.exports = WorkerController;
