const fs = require('fs');
const json2csv = require('json2csv');

function saveAsCSV(data, path) {
  fs.writeFileSync(path, data);
}

function exportAsCSV(data, fields, path) {
  const csv = json2csv({ data, fields });
  saveAsCSV(csv, path);
}

module.exports = exportAsCSV;
