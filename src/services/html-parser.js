const requestPromise = require('request-promise-native');
const cheerio = require('cheerio');
const urlModule = require('url');

/**
 * Removes end slash from a url. It helps in decreasing duplication of an URL.
 * Eg. https://medium.com === https://medium.com/
 *
 * @param {string} hyperlink - a url
 */
function trimEndSlash(hyperlink) {
  return hyperlink.replace(new RegExp('/$'), '');
}

/**
 * Initializes an instance of cheerio
 *
 * @param {string} body
 */
function transformBodyToCheerio(body) {
  return cheerio.load(body);
}

// default options for every request.
const httpClient = requestPromise.defaults({
  // middleware to transform response body to cheerio object.
  transform: transformBodyToCheerio,
  followRedirect: false,
  gzip: true,
});

class HTMLParser {

  /**
   * Fetches HTML and initialize a instance of HTMLParser.
   *
   * @param {string} url - url to fetch HTML
   */
  static async initializeAsync(url) {
    try {
      const cheerioObj = await httpClient.get(url);
      return new HTMLParser(url, cheerioObj);
    } catch (ex) {
      console.log(`Error occur: URL -> ${url}\n${ex.message}`);
      return null;
    }
  }

  /**
   * @constructor
   *
   * @param {string} url - the url of the response which loaded into the cheerio instance
   * @param {CheerioStatic} cheerio - instance of cheerio
   */
  constructor(url, cheerio) {
    this.url = new urlModule.URL(url);
    this.$ = cheerio;
  }

  /**
   * Returns all the hyperlinks which points to the same host.
   *
   * @return {Set<string>} - all hyperlinks
   */
  get allHyperlinks() {
    return new Set([
      ...(this.absoluteHyperlinks),
      ...(this.relativeHyperlinks),
    ]);
  }

  /**
   * Returns the list of relative hyperlinks
   *
   * @return {Set<string>} - all relative hyperlinks, already converted to absolute.
   */
  get relativeHyperlinks() {
    const $ = this.$;
    const self = this;
    const hyperlinks = new Set();

    // find all the alink start with '/'
    $('a[href^=\'//\']').remove().find('a[href^=\'/\']').each(function() {
      const hyperlink = self.relativeToAbsoluteLink(
        trimEndSlash($(this).attr('href'))
      );
      hyperlinks.add(hyperlink);
    });

    return hyperlinks;
  }

  /**
   * Returns the list of hyperlinks which points to the same host.
   *
   * @return {Set<string>} - all absolute hyperlinks
   */
  get absoluteHyperlinks() {
    const $ = this.$;
    const hyperlinks = new Set();

    // find all alink start with the origin of the url.
    $(`a[href^='${this.url.origin}']`).each(function () {
      const hyperlink = trimEndSlash($(this).attr('href'));
      hyperlinks.add(hyperlink);
    });

    return hyperlinks;
  }

  /**
   *
   * @param {string} hyperlink - scraped relative path
   */
  relativeToAbsoluteLink(hyperlink) {
    return urlModule.resolve(this.url.origin, hyperlink);
  }
}

module.exports = HTMLParser;
