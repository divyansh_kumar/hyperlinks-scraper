const HTMLParser = require('./html-parser');

const WorkerController = process.env.WITHOUT_ASYNC
                            ? require('./worker-without-async')
                            : require('./worker-async');

/**
 * Scrapes all the hyperlinks which points to the same host
 *
 * @param {string} url - the target website url
 * @param {number} concurrencyRate - the rate of concurrency
 * @return {Promise} - resolves with list of hyperlinks
 */
function listHyperlinksAsync(url, concurrencyRate) {

  // It helps in maintaining uniqueness of URL
  // to avoid re-scraping of the same hyperlink.
  const hyperlinkCollection = new Set();

  const worker = new WorkerController((hyperlink) =>
    HTMLParser.initializeAsync(hyperlink)
      .then(instance => {
        if (instance) {
          hyperlinksScraper(instance.allHyperlinks);
        }
        return hyperlink;
      })
  , concurrencyRate);

  /**
   * Assigns scraping task to worker for a hyperlink.
   * It performs recursively.
   *
   * @param {Set|Array} hyperlinks - scraper
   */
  function hyperlinksScraper(hyperlinks) {
    for (const hyperlink of hyperlinks) {
      // Skip if the hyperlink has already processed
      if (hyperlinkCollection.has(hyperlink)) {
        continue;
      }
      // Add hyperlink to the collection
      hyperlinkCollection.add(hyperlink);
      worker.push(hyperlink);
    }
  }

  return new Promise((resolve) => {
    // Resolve the promise on completion with list of hyperlinks.
    let resolver = () => resolve(hyperlinkCollection);

    // resolves on worker drain
    worker.onDrain(() => {
      if (!resolver) return;
      resolver();
      resolver = null;
    });

    // resolves on ctrl + c press
    process.on('SIGINT', function() {
      if (!resolver) return;
      resolver();
      resolver = null;
    });
    hyperlinksScraper([ url ]);
  });
}

module.exports = listHyperlinksAsync;
